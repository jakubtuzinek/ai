<?php

namespace App\Form;

use App\Entity\Measuerement;
use App\Entity\City;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class MeasuerementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('city_name')
            ->add('date', DateType::class)
            ->add('temperature', NumberType::class, [
                'html5' => true,
            ])
            ->add('humidity', NumberType::class, [
                'html5' => true,
            ])
            ->add('chance_of_precision', NumberType::class, [
                'html5' => true,
            ])
            ->add('wind_speed', NumberType::class, [
                'html5' => true,
            ])
            ->add('description')
            ->add('city', EntityType::class, [
                'class' => City::class,
                'choice_label' => 'name',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Measuerement::class,
        ]);
    }
}
