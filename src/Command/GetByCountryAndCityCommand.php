<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\WeatherUtil;

class GetByCountryAndCityCommand extends Command
{
    protected static $defaultName = 'app:getByCountryAndCity';
    protected static $defaultDescription = 'Get data by country name and city name';

    private $weatherUtil;

    public function __construct(WeatherUtil $weatherUtil)
    {
        $this->weatherUtil = $weatherUtil;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('country', InputArgument::REQUIRED, 'Country name')
            ->addArgument('city', InputArgument::REQUIRED, 'City name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $country = $input->getArgument('country');
        $city = $input->getArgument('city');
        $measuerements = $this->weatherUtil->getWeatherForCountryAndCity($country, $city);

        $output->writeln($measuerements[0]->getCity()->getName());
        foreach($measuerements as $meas)
        {
            $output->writeln($meas->getDate()->format('Y.m.d') . ': ' . $meas->getTemperature());
        }

        return 0;
    }
}
