<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Service\WeatherUtil;

class GetByIdCommand extends Command
{
    protected static $defaultName = 'app:getById';
    protected static $defaultDescription = 'Get data by ID';
    private $weatherUtil;

    public function __construct(WeatherUtil $weatherUtil)
    {
        $this->weatherUtil = $weatherUtil;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('id', InputArgument::REQUIRED, 'City id')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $city = $input->getArgument('id');
        $measuerements = $this->weatherUtil->getWeatherForLocation($city);

        $output->writeln($measuerements[0]->getCity()->getName());
        foreach($measuerements as $meas)
        {
            $output->writeln($meas->getDate()->format('Y.m.d') . ': ' . $meas->getTemperature());
        }

        return 0;
    }
}
