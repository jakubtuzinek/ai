<?php

namespace App\Repository;

use App\Entity\City;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method City|null find($id, $lockMode = null, $lockVersion = null)
 * @method City|null findOneBy(array $criteria, array $orderBy = null)
 * @method City[]    findAll()
 * @method City[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, City::class);
    }

    // /**
    //  * @return City[] Returns an array of City objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?City
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param $name
     */

    public function findCityByName($name)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.name = :name')
        ->setParameter('name', $name);
        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * @param $country
     * @param $name
     */

    public function findByCityAndCountry($country, $name)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.country = :country')
        ->setParameter('country', $country)
        ->andWhere('m.name = :name')
        ->setParameter('name', $name);
        $query = $qb->getQuery();
        return $query->getSingleResult();
    }

}
