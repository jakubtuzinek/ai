<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Measuerement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Measuerement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Measuerement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Measuerement[]    findAll()
 * @method Measuerement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MeasuerementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Measuerement::class);
    }

    // /**
    //  * @return Measuerement[] Returns an array of Measuerement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Measuerement
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * @param City $city
     */

    public function findByCity($city)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->where('m.city = :city')
        ->setParameter('city', $city);
        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }
}
