<?php // src/Service/WeatherUtil.php

namespace App\Service;

use App\Repository\CityRepository;
use App\Entity\City;
use App\Repository\MeasuerementRepository;
use App\Entity\Measuerement;

class WeatherUtil

{
    private CityRepository $cityRepository;
    private MeasuerementRepository $measuerementRepository;

    /**
    * @param $cityRepository
    * @param $measuerementRepository
    */
    function __construct(CityRepository $cityRepository, MeasuerementRepository $measuerementRepository)
    {
        $this->cityRepository = $cityRepository;
        $this->measuerementRepository = $measuerementRepository;
    }

    /**
    * @param string $country
    * @param string $name
    */
    public function getWeatherForCountryAndCity($country, $name)
    {
        $city = $this->cityRepository->findByCityAndCountry($country, $name);
        return $this->getWeatherForLocation($city);
    }

     /**
     * @param City $city
     */
    public function getWeatherForLocation($city)
    {
        return $this->measuerementRepository->findByCity($city);
    }

}