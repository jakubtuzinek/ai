<?php

namespace App\Controller;

use App\Entity\Measuerement;
use App\Form\MeasuerementType;
use App\Repository\MeasuerementRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/measuerement")
 */
class MeasuerementController extends AbstractController
{
    /**
     * @Route("/", name="measuerement_index", methods={"GET"})
     */
    public function index(MeasuerementRepository $measuerementRepository): Response
    {
        return $this->render('measuerement/index.html.twig', [
            'measuerements' => $measuerementRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="measuerement_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $measuerement = new Measuerement();
        $form = $this->createForm(MeasuerementType::class, $measuerement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($measuerement);
            $entityManager->flush();

            return $this->redirectToRoute('measuerement_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('measuerement/new.html.twig', [
            'measuerement' => $measuerement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="measuerement_show", methods={"GET"})
     */
    public function show(Measuerement $measuerement): Response
    {
        return $this->render('measuerement/show.html.twig', [
            'measuerement' => $measuerement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="measuerement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Measuerement $measuerement): Response
    {
        $form = $this->createForm(MeasuerementType::class, $measuerement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('measuerement_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('measuerement/edit.html.twig', [
            'measuerement' => $measuerement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="measuerement_delete", methods={"POST"})
     */
    public function delete(Request $request, Measuerement $measuerement): Response
    {
        if ($this->isCsrfTokenValid('delete'.$measuerement->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($measuerement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('measuerement_index', [], Response::HTTP_SEE_OTHER);
    }
}
