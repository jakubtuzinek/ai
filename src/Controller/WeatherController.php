<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\City;
use App\Repository\MeasuerementRepository;
use App\Repository\CityRepository;
use App\Service\WeatherUtil;

class WeatherController extends AbstractController
{
    public function cityAction($country, $city, WeatherUtil $weatherUtil): Response
    {
        $measuerements = $weatherUtil->getWeatherForCountryAndCity($country, $city);

        return $this->render('weather/city.html.twig', [
            'city' => $measuerements[0]->getCity(),
            'measuerements' => $measuerements,
        ]);
    }
}